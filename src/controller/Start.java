package controller;

import gui.Index;

/**
 *
 * @author Luis Ventura
 */

public class Start {
    public static void main(String[] args) {
        Index index = new Index();
        index.setVisible(true);
    }
}
