package controller;

import bean.Conditions;
import javax.swing.JTextArea;

/**
 *
 * @author Luis Ventura
 */

public class Calc {

    public Calc(int horas, double pagoH, JTextArea txaResult) {
        Conditions conditions = new Conditions();
        conditions.setPagoHora(pagoH);
        conditions.setPagoExtra(pagoH * 2);

        int horasExtra = horas - 40;

        if (horasExtra < 0) {
            conditions.setHoras(horas);
            conditions.setHorasExtras(0);
        } else {
            conditions.setHoras(40);
            conditions.setHorasExtras(horasExtra);
        }

        double sueldoBase = conditions.getHoras() * conditions.getPagoHora();
        double sueldoExtra = conditions.getHorasExtras() * conditions.getPagoExtra();

        double sueldoMedio = sueldoBase + sueldoExtra;

        if (sueldoMedio >= 20000) {
            conditions.setImpuestos(0.25 * sueldoMedio);
        } else if (sueldoMedio >= 15000) {
            conditions.setImpuestos(0.20 * sueldoMedio);
        } else if (sueldoMedio >= 10000) {
            conditions.setImpuestos(0.10 * sueldoMedio);
        } else {
            conditions.setImpuestos(0);
        }

        conditions.setSueldo(sueldoMedio - conditions.getImpuestos());

//        System.out.println("Base " + sueldoBase);
//        System.out.println("Extra " + sueldoExtra);
//        System.out.println("Impuesto " + conditions.getImpuestos());

        showResult(conditions, txaResult);

    }

    private void showResult(Conditions conditions, JTextArea txaResult) {
        String result = "Sueldo Neto: S/." + conditions.getSueldo() + "\n\n"
                + "Por Horas Extra: S/." + conditions.getPagoExtra() * conditions.getHorasExtras() + "\n"
                + "Impuestos: S/." + conditions.getImpuestos();
        txaResult.setText(result);
    }

}
