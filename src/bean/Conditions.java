package bean;

/**
 *
 * @author Luis Ventura
 */

public class Conditions {
    public double sueldo, impuestos, pagoExtra, pagoHora;

    public int horas, horasExtras;

    public Conditions() {
    }

    public Conditions(double sueldo, double impuestos, double pagoExtra, double pagoHora, int horas, int horasExtras) {
        this.sueldo = sueldo;
        this.impuestos = impuestos;
        this.pagoExtra = pagoExtra;
        this.pagoHora = pagoHora;
        this.horas = horas;
        this.horasExtras = horasExtras;
    }

    public void setPagoExtra(double pagoExtra) {
        this.pagoExtra = pagoExtra;
    }

    public void setPagoHora(double pagoHora) {
        this.pagoHora = pagoHora;
    }

    public double getPagoExtra() {
        return pagoExtra;
    }

    public double getPagoHora() {
        return pagoHora;
    }
    
    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public double getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(double impuestos) {
        this.impuestos = impuestos;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }
    
}
